%% noiseAnalysis.m
clear; clc; close all;
%% Initialize
[rawData,triggerSignal,sampleRate,channelNames,filterInfo,daqInfos]=loadSessionData();
[numSamples,numChannels] = size(rawData);
channelNames = {' FZ',' FC1',' FC2',' C3',' CZ',' C4',' CP1',' CP2'};
%load('experimentParameters.mat');

load('frontEndFilter.mat');
filteredData =  filter(frontendFilter.Num,1,[rawData; zeros(frontendFilter.groupDelay,size(rawData,2))]);
filteredData = filteredData(frontendFilter.groupDelay+1:end,:);

figure (1); plot(triggerSignal);

%% Fix trigger data...
% diffTrigger = [triggerSignal(2:end); 0] - triggerSignal;
% diffTrigger(find(diffTrigger < 0)) = 0;
repetitions = [1 10 10];
signalDuration = [1 10 10];
pauseDuration = [0 0.5 0.5];
nEpochs = 3;

%% Seperate Data into the 3 different test modes
% triggerIdx = find(diffTrigger > 0);
% timeIdx{1} = triggerIdx(1):(triggerIdx(10)+(10*sampleRate));
% segmentedData{1} = filteredData(timeIdx{1},:);
% timeIdx{2} = triggerIdx(11):(triggerIdx(20)+(10*sampleRate));
% segmentedData{2} = filteredData(timeIdx{2},:);
% timeIdx{3} = triggerIdx(21):(triggerIdx(30)+(10*sampleRate));
% segmentedData{3} = filteredData(timeIdx{3},:);
for i = 1:nEpochs
    triggerStart(i) = min(find(triggerSignal == i));
    triggerEnd(i) = max(find(triggerSignal == i));
    %triggerEnd(i) = triggerEnd(i) + signalDuration(i)*sampleRate;
    segmentedData{i} = filteredData(triggerStart(i):triggerEnd(i),:);
    signalLength(i) = length(triggerStart(i):triggerEnd(i));
end


%% Spectral Analysis
pds = cell(nEpochs,numChannels);
for i = 1:nEpochs
    NFFT(i) = 2^nextpow2(signalLength(i));
    for j = 1:numChannels
        signal = segmentedData{i};
        pds{i,j} = fft(signal(:,j),NFFT(i))/signalLength(i);
    end
end

%% Plot
figureTitles = {' At Rest',' Tactor Constant',' Tactor Pulsed'};

for j = 1:numChannels
    figure (j+1);
    for i = 1:nEpochs
        
        %pulseLength = triggerIdx(1+10*(i-1)):triggerIdx(3+10*(i-1));
        transientTimePlot = triggerStart(i):triggerStart(i)+(2*signalDuration(i)+pauseDuration(i))*sampleRate;
        currentFFT = pds{i,j};
        f = sampleRate/2*linspace(0,1,NFFT(i)/2+1);
        
        % Transient
        subplot(3,2,1+2*(i-1));
        plot((1:length(transientTimePlot))/sampleRate,filteredData(transientTimePlot,j),'b');
        %plot((1:length(pulseLength))/sampleRate,diffTrigger(pulseLength)*1e-04,'r');
        title(strcat('EEG Transient: ',figureTitles{i},' - Channel ',channelNames{j}));
        set(gca,'FontSize',12,'FontWeight','bold');
        xlabel('Time (Seconds)');
        ylabel('EEG Signal (Volts)');
        axis([0 (2*signalDuration(i)+pauseDuration(i)) -1.2e-04 1.2e-04]);
        grid on;
        
        %PDS
        subplot(3,2,2*i);
        plot(f,10*log10(abs(currentFFT(1:NFFT(i)/2+1))));
        title('Power Density Spectrum');
        set(gca,'FontSize',12,'FontWeight','bold');
        xlabel('Frequency (Hz)');
        ylabel('Power (dB)');
        axis([0 sampleRate/2 -110 -50]);
        grid on;
    end
end
    
    
    
    
    