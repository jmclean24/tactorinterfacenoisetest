%% setPathandFilenames

%% add entire code directory to path using a targetFile in highest directory
% of code
targetFile = 'sessionSpeller.m';
mainCodeDirectory = which(targetFile);
mainCodeDirectory = mainCodeDirectory(1:end-length(targetFile));
addpath(genpath(mainCodeDirectory));
rmpath(genpath([mainCodeDirectory,'\.git']));

%% add data folder if need be
dataDirectory = [mainCodeDirectory 'Data\'];
if ~exist(dataDirectory,'dir'), mkdir(dataDirectory), end

sessionTag = [projectID,'_',subjectID,'_',datestr(now,'mmm-dd-yyyy_HH-MM_AM')];

%% create a unique folder to store data in
% (assumes that < 8 folder with identical sessionTags exist)
idx = 1;
while exist([dataDirectory,sessionTag],'dir')
    if idx == 1
        sessionTag = [sessionTag, '(2)']; %#ok<AGROW>
    else
        sessionTag = [sessionTag(1:end-3) '(',num2str(idx),')'];
    end
    idx = idx+1;
end
mkdir([dataDirectory,sessionTag]);

%% genericRecordFilename is a prefix for all record files
genericRecordFilename = [dataDirectory,sessionTag,'\',sessionTag];
