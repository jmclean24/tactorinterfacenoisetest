%% tactorInterfaceNoiseTest.m
clear; clc;

%% initialize gUSBManager Object
projectID = '200HzStimulationTest';
sessionID = '512HzSampleRate';
subjectID = '';

setPathandFilenames;
DAQParameters;

% gUSBManagerObj = gUSBManager(...
%     'fs',fs,...
%     'notchFilterNdx', notchFilterNdx,...
%     'ampFilterNdx', ampFilterNdx,...
%     'calibrationFlag', calibrationFlag,...
%     'testParalellPortFlag',testParalellPortFlag,...
%     'ampBufferLengthSec', ampBufferLengthSec,...
%     'channelParametersFilename','channels.csv',...
%     'genericRecordFilename',genericRecordFilename);
% 
% loadlibrary('inpout32','inpout32.h');
% decParallelPortNumber =  gUSBManagerObj.decParallelPortNumber;
daqType = 'gUSBAmp';
switch daqType
    case 'gUSBAmp'
        daqManagerObj = gUSBManager(...
            'fs',fs,...
            'notchFilterNdx', notchFilterNdx,...
            'ampFilterNdx', ampFilterNdx,...
            'calibrationFlag', calibrationFlag,...
            'testParalellPortFlag',testParalellPortFlag,...
            'ampBufferLengthSec', ampBufferLengthSec,...
            'channelParametersFilename','channels.csv',...
            'genericRecordFilename',genericRecordFilename);
        
        loadlibrary('inpout32','inpout32.h');
        decParallelPortNumber =  daqManagerObj.decParallelPortNumber;
    case 'noAmp'
        daqManagerObj = noAmpManager(...
            'fs',fs,...
            'channelParametersFilename','channels.csv',...
            'genericRecordFilename',genericRecordFilename);
    otherwise
        error('invalid daqType');
end

%% Run GUI
preLaunchGUI;

%% Initialize Tactor Amplifier
samplingRate = 44100; %Hz
numChannels = 2;
deviceID = []; %see getdevices option of PsychPortAudio function
mode = 1; %playback only
reqLatencyClass = 1; %lowest possible latency  within system constraints
bufferSize = [];

TactorStimulationObj = TactorStimulation('samplingRate',samplingRate,...
                                         'numChannels',numChannels,...
                                         'deviceID',deviceID,...
                                         'mode',mode,...
                                         'reqLatencyClass',reqLatencyClass,...
                                         'bufferSize',bufferSize);

%% Start Initial Test
button = questdlg('Remain at rest for 10 seconds without touching the tactors. The test begins when you press YES.');

%noAmpManagerObj.startAmps; %START RECORDING DATA HERE
daqManagerObj.startAmps;
calllib('inpout32','Out32',decParallelPortNumber,0);
WaitSecs(10);
calllib('inpout32','Out32',decParallelPortNumber,0);
repetitions(1) = 1;
signalDuration(1) = 10;
%% Have user input experiment parameters
loopFlag = 1;
j = 2;
while (loopFlag == 1);
    
    %open dialogue prompt
    prompt = {'CONSTANT or PULSED:',...
              'Waveform Frequency (Hz):',...
              'Waveform Amplitude:',...
              'Signal Repetitions:',...
              'Waveform Length (seconds):',...
              'Waveform Pause (seconds):',...
              'Pulse Signal Frequency (Hz):',...
              'Pulse Pause Frequency (Hz):'};
    defaultanswer = {'PULSED','200','1','3','1','1','25','25'};
    waveformParameters = inputdlg(prompt,'Stimulation Parameters',1,...
                                  defaultanswer);
                              
    tactorFreq(j+1) = str2double(waveformParameters{2});
    tactorAmp(j+1) = str2double(waveformParameters{3});
    repetitions(j+1) = str2double(waveformParameters{4});
    signalDuration(j+1) = str2double(waveformParameters{5});
    pauseDuration(j+1) = str2double(waveformParameters{6});
    
    switch (waveformParameters{1})
        
        case ('CONSTANT')

            waveform = tactorAmp*sin(2*pi*tactorFreq*(0:1/samplingRate:signalDuration));
            waveform(2,:) = waveform;
            pulseRepetitions = 1;
            
        case ('PULSED')
            
            pulseLength(j+1) = 1/str2double(waveformParameters{7});
            pulsePause(j+1) = 1/str2double(waveformParameters{8});
            signal = tactorAmp*sin(2*pi*tactorFreq*(0:1/samplingRate:pulseLength));
            waveform = cat(2,signal,zeros(1,pulsePause*samplingRate));
            waveform(2,:) = waveform;
            pulseRepetitions = floor(signalDuration/(pulseLength+pulsePause));
            
    end
    
    TactorStimulationObj.setWaveform(waveform);
    i = 0;
    
    while i < repetitions
    
        calllib('inpout32','Out32',decParallelPortNumber,j);
        TactorStimulationObj.Start(pulseRepetitions,[1 2]); %TRIGGER ON
        TactorStimulationObj.Stop(); %TRIGGER OFF
        calllib('inpout32','Out32',decParallelPortNumber,0);
        WaitSecs(pauseDuration);

        i = i + 1;
        
    end
    
    endTestprompt = {'Enter 0 to end test, enter 1 to run again.'};
    loopFlag = str2double(inputdlg(endTestprompt));
    
    j = j + 1;

end

numEpochs = j;
% Save parameters

save(strcat(pwd,'\Data\experimentParameters.mat'),numEpochs,...
                                                  repetitions,...
                                                  signalDuration,...
                                                  pauseDuration);



